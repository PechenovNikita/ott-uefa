function openRules() {
  console.log('open');
  document.getElementById('rules').className = 'b-rules show';
}

function closeRules() {
  console.log('close');
  document.getElementById('rules').className = 'b-rules';
}
Array.prototype.slice.call(document.querySelectorAll('.js-open')).forEach(function (el) {
  el.addEventListener('click', openRules);
});
Array.prototype.slice.call(document.querySelectorAll('.js-close')).forEach(function (el) {
  el.addEventListener('click', closeRules);
});
document.addEventListener('keydown', function (event) {
  if (event.keyCode == 27)
    closeRules();
});