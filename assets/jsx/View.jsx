var View = (function () {
  'use strict';

  var View = React.createClass({

    getInitialState:function () {
      this._resizeFooter = true;
      return {
        start             : false,
        finish            : false,
        questions         : [],
        wrap_footer_style : {}
      };
    },
    componentDidMount : function () {
      mixins.addAniEnd(document.getElementById('app'), this.animationEnd);
      window.addEventListener('resize', this.__resizeWindow);
      this.__resizeWindow();

      if (!Controller.animation)
        this.__initMain();
    },
    __resizeWindow:function (event){
      if (!this._resizeFooter)
        return true;
      var st = this.state;
      st.wrap_footer_style = {};
      this.setState(st);

      var winH = window.innerHeight
        , headH = this.refs.wrap_header.offsetHeight
        , bodyH = this.refs.wrap_body.offsetHeight
        , footH = this.refs.wrap_footer.offsetHeight;

      if (winH > (headH + footH + bodyH)) {
        st.wrap_footer_style = {height : (winH - headH - bodyH)};
        this.setState(st);
      }
      return true;
    },
    __resetFooterHeight:function (){
      var st = this.state;
      st.wrap_footer_style = {padding : 0};
      this.setState(st);
    },
    animationEnd:function (event) {
      //console.log('animationEnd stopAnimation:', event.animationName);
      Controller.animation = true;
      switch (event.animationName) {
        case 'footer_show':
          this.__initMain();
          break;
        case 'footer_hide':
          this.__disableMain();
          break;
        case 'questionnaire__show':
        case 'show_question':
          Controller.needAnswer();
          this.refs.questionnaire.displayQuestion();
          break;
      }
    },
    __initMain:function (){
      this.refs.footer.initLogo();
    },
    __disableMain:function (){
      this.__resetFooterHeight();
      this.refs.header.hide();
      this.refs.footer.hide();
      this._resizeFooter = false;
    },
    start:function () {
      //console.log('View start');
      var st = this.state;
      st.start = true;
      st.questions = Controller.getQuestions();
      this.setState(st);
      this.refs.header.start();
      if (!Controller.animation)
        this.__disableMain();
    },
    next:function (){
      //console.log('View next');
      this.refs.questionnaire.next();
    },
    finish:function (){
      var st = this.state;
      st.finish = true;
      this.setState(st);
      this.refs.header.disable();
    },
    shareResult:function (type){
      //console.log('ShareResult');
      Controller.shareResult(type);
      setTimeout(this.refs.popup.form, 3000);
    },
    showRules:function (){
      //console.log('showRules', this.refs);
      this.refs.rules.show();
    },
    render:function () {
      var cl = (this.state.start ? 'app-start' : '') + (this.state.finish ? ' app-finish' : '');
      var questionnaire = (this.state.start
        ? (<Questionnaire ref="questionnaire" controller={this} questions={this.state.questions}/>)
        : []);
      var popup = (this.state.finish
        ? <Final ref="popup" controller={this}/>
        : []);

      return (
        <div className={cl}>
          <div ref="wrap_header" className="wrapper__header">
            <Header ref="header" controller={this}/>
          </div>
          <div ref="wrap_body" className="wrapper__body">
            <div className="wrapper__body__questionnaire">
              {questionnaire}
            </div>
            <div className="wrapper__body__popup">
              {popup}
            </div>
          </div>
          <div ref="wrap_footer" className="wrapper__footer" style={this.state.wrap_footer_style}>
            <Footer ref="footer" controller={this}/>
          </div>

          <Rules ref="rules"/>
        </div>
      );
    }

  });

  return {
    init:function (){
      ReactDOM.render(<View/>, document.getElementById('app'));
    }
  };
}());
