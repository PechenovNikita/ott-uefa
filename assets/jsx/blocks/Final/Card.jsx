var Card = (function () {
  'use strict';

  return React.createClass({
    render:function () {
      var cl = 'b-card' +
        (this.props.result >= 2 ? (this.props.result >= 4 ? ' blue' : ' yellow') : ' red');
      return (
        <div className={cl}>
          <div className="b-card__top">
            <div className="b-card__result">
              { this.props.result} из 5
            </div>
            <div className="b-card__title">
            <span className="b-card__title__inner">
              {this.props.title}
            </span>
            </div>
            <div className="b-card__text">
            <span className="b-card__text__inner">
              {this.props.text}
            </span>
            </div>
          </div>
          <div className="b-card__bottom">
            <div className="b-card__prize">
              <Prize euro={true}/>
            </div>
            <div className="b-card__logos">
              <div className="b-card__logos__logo ott">
                <img src={config.path.img+"final/ott.png"} className="b-header__logos__logo__ott__img"/>
              </div>
              <div className="b-card__logos__logo champ">
                <img src={config.path.img+"final/champ.png"} className="b-header__logos__logo__ott__img"/>
              </div>
            </div>
          </div>
        </div>
      );
    }
  });
}());
