var Final = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      return {
        social  : true,
        form    : false,
        done    : false,
        error   : false,
        disable : false
      };
    },
    componentDidMount : function () {

    },
    form:function (){
      var st = this.state;
      st.form = true;
      this.setState(st);
    }, 
    email:function (event){
      this.disable();
      event.stopPropagation();
      event.preventDefault();
      var email = this.refs.email_input.value;

      if (this.validateEmail(email)) {
        this.validateEmailByAjax(email);
      } else
        this.formError();

    },
    validateEmail:function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    validateEmailByAjax:function (email){
      Ajax.jsonP(config.valideEmail, {email : email}, this.__emailCheckAjax);
    },
    __emailCheckAjax:function (response){
      if (response && response.isValid)
        Ajax.jsonP(config.confirm, {
          source      : config.source,
          email       : this.refs.email_input.value,
          redirectURL : config.redirectURL
        }, this.done);
      else
        this.formError();
    },
    formError:function (){
      this.undisable();
      var st = this.state;
      st.error = true;
      this.setState(st);
    },
    formClear:function (){
      var st = this.state;
      st.error = false;
      this.setState(st);
    },
    done:function (response){
      // console.log(response);
      if (typeof response == 'string')
        response = JSON.parse(response);
      if (!response || response.result != 'OK') {
        this.formError();
      } else {
        window.location = config.redirectURL;
      }
    },
    disable:function (){
      var st = this.state;
      st.disable = true;
      this.setState(st);
    },
    undisable:function (){
      var st = this.state;
      st.disable = false;
      this.setState(st);
    },
    render:function () {

      var result = Controller.getResult();
      var text = Controller.getTitle();

      var cl = "b-final"
        + ((this.state.form) ? " form" : "")
        + ((this.state.done) ? " done" : "");

      var title = (this.state.form ? "Спасибо за участие!" : "Ваш результат");

      return (
        <div className={cl}>

          <div className="b-final__social">
            <div className="b-final__social__title">
              <div className="b-final__social__title__inner">Поделитесь карточкой в социальных сетях, чтобы принять
                участие в конкурсе
              </div>
            </div>
          </div>
          <div className="b-final__title">
            <span className="b-final__title__inner">{title}</span>
          </div>
          <div className="b-final__card">
            <Card result={Controller.getResult()} title={Controller.getTitle()} text={Controller.getSubText()}/>
          </div>
          <div className="b-final__social card">
            <Socials result={true} blue={true} controller={this.props.controller}/>
          </div>
          <form onSubmit={this.email} onInput={this.formClear}
                ref="email_form" method="post"
                action={config.confirm}
                className={"b-final__form"+(this.state.error?' error':"")}>
            <input type="hidden" name="source" value="promo_euro2016"/>
            <input type="hidden" name="redirectURL" value="https://www.onetwotrip.com/promo/newyear2016/confirm.html"/>
            <div className="b-final__form__desc">
            <span
              className="b-final__form__desc__inner">Оставьте свой электронный адрес и узнайте о результатах соревнования</span>
            </div>
            <div className="b-final__form__control">
              <input ref="email_input" type="text" name="email" placeholder="Введите электронный адрес"
                     className="b-final__form__control__input"/>
            </div>
            <div className="b-final__form__submit">
              <input className="b-final__form__submit__btn" disabled={this.state.disable ? 'disabled' : ''}
                     type="submit"
                     value="Отправить"/>
            </div>
          </form>
          <div className="b-final__social form">
            <Socials result={true} blue={true} controller={this.props.controller}/>
          </div>
          <div className="b-final__prize">
            <div className="b-final__prize__title">
              <span className="b-final__prize__title__inner">Главный приз</span>
            </div>
            <Prize euro={true}/>
          </div>
          <div className="b-final__rules">
            <a href="#" onClick={this.props.controller.showRules} className="b-footer__rules__link"><span
              className="b-footer__rules__link__inner">Правила конкурса</span></a>
          </div>
        </div>
      );
    }
  });
}());
