var Vid = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      return {
        play : false
      };
    },
    componentDidMount : function () {
      this.refs.video.addEventListener('play', this.onPlay);
      this.refs.video.addEventListener('pause', this.onPause);
    },
    play:function (){
      this.refs.video.play();
    },
    onPlay:function (){
      this.setState({
        play : true
      });
    },
    onPause:function (){
      this.setState({
        play : false
      });
    },
    pause:function (){
      this.refs.video.pause();
    },
    repeat:function (){
      this.refs.video.currentTime = 0;
      this.play();
    },
    render:function () {
      // console.log(this.props.poster);
      var sources = this.props.src.map(function (src, index) {
        return <source src={src.src} type={src.type} key={index}/>
      });
      return (
        <div className="b-video">
          <div className={"b-video__play"+(this.state.play?' hide':'')} onClick={this.play}>
            <div className="b-video__play__btn"></div>
          </div>
          <video ref="video" poster={this.props.poster} className="b-video__media">
            {sources}
          </video>
        </div>
      );
    }
  });
}());
