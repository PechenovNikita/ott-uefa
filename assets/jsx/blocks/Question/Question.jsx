var Question = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      //console.log('init Question');
      return {
        display    : false,
        current    : this.props.current,
        prev       : this.props.prev,
        next       : this.props.next,
        loadVideo  : false,
        startVideo : false,
        answered   : false
      };
    },
    componentWillReceiveProps:function (props){
      // console.log('Question componentWillReceiveProps');

      var st = this.state;
      st.current = props.current;
      st.prev = props.prev;
      this.setState(st);
    },
    componentDidMount:function () {

    },
    questionHeight:function (){
      return this.refs.media.offsetHeight + this.refs.text.offsetHeight;
    },
    display:function (){
      var st = this.state;
      st.display = true;
      this.setState(st);
      setTimeout(this.__loadVideo, 300);
    },

    __loadVideo:function (){
      var st = this.state;
      st.loadVideo = true;
      this.setState(st);
    },
    answer:function (){
      var st = this.state;
      st.answered = true;
      this.setState(st);
      setTimeout(this.showVideo, 300);
    },
    showVideo:function (){
      var st = this.state;
      st.startVideo = true;
      this.setState(st);
      this.refs.video.play();
    },
    repeat:function (){
      this.refs.video.repeat();
    },
    next:function (){
      // console.log('Question next');
      this.refs.video.pause();
      this.props.controller.next();
    },

    height:function (){
      return this.refs.main.offsetHeight;
    },
    render:function () {

      var questionClass = "b-question" +
        (this.state.display ? " visible" : '') +
        (this.state.current ? " current" : "") +
        (this.state.prev ? " prev" : "") +
        (this.state.next ? " next" : "" ) +
        (this.state.answered ? " answered" : "");

      var video = (this.state.loadVideo ?
        <Vid ref="video" poster={this.props.videoPoster} src={this.props.video}/> : [])
        , socials = (this.state.startVideo ? <Socials blue="true"/> : '');

      return (
        <div ref="main" className={questionClass}>
          <div className="b-question__media" ref="media">
            <div className="b-question__media__photo">
              <div className="b-question-photo">
                <div className="b-question-photo__back"></div>
                <div className="b-question-photo__img"
                     style={{backgroundImage:"url('"+this.props.photo+"')"}}></div>
              </div>
            </div>
            <div className="b-question__media__video">
              <div className="b-question-video">
                {video}
              </div>
              <div className="b-question__media__video__links">
                <div className="b-question__media__video__links__btn">
                  <button onClick={this.repeat}>Повторить</button>
                </div>
                <div className="b-question__media__video__links__btn">
                  {socials}
                </div>
                <div className="b-question__media__video__links__btn">
                  <button onClick={this.next}>{this.props.last ? "Результат" : "Следующий" }</button>
                </div>
              </div>
            </div>
          </div>
          <div className="b-question__text" ref="text">
            <div className="b-question-text">
              <span className="b-question-text__inner">{this.props.text}</span>
            </div>
          </div>
        </div>
      )
    }
  });
}());
