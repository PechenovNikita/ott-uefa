var QuestionsView = (function () {
  'use strict';
  
  return React.createClass({
    getInitialState:function () {
      //console.log('init QuestionsView');
      this._current = 0;
      return {
        current : 0
      };
    },
    display:function (){
      this.refs['question_' + this._current].display();
    },
    showVideo:function (){
      this.refs['question_' + this._current].answer();
    },
    next:function (){
      this._current = this.state.current + 1;
      var st = this.state;
      st.current = this._current;
      this.setState(st);
    },
    render:function () {
      var self = this;
      var questions = this.props.questions.map(function (question, index, qs) {
        return <Question key={index}
                         ref={'question_'+index}
                         controller={self.props.controller}

                         prev={index < self.state.current}
                         current={index === self.state.current}
                         next={index > self.state.current}
                         last={index == qs.length - 1}

                         photo={question.photo}
                         videoPoster={question.videoPoster}
                         video={question.video}
                         text={question.question}/>;
      });
      return (
        <div className="b-questions-view">
          {questions}
        </div>
      );
    }
  });
}());
