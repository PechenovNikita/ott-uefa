var AnswerBtn = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      //console.log('init AnswerBtn');
      return {
        answered : false
      };
    },
    click:function (){
      if (Controller.answered())
        return;
      this.setState({
        answered : true
      });
      if (this.props.onSelect)
        this.props.onSelect(this.props.right);
    },
    render : function () {
      var cl = "b-answer_btn" +
        (this.state.answered ? (this.props.right ? ' right' : ' false') : '');

      return (
        <div className={cl} onClick={this.click}>
          <div className="b-answer_btn__icon"></div>
          <div className="b-answer_btn__text">{this.props.text}</div>
        </div>
      );
    }
  });

}());
