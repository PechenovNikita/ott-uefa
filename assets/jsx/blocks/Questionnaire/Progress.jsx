var Progress = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      //console.log('init Progress');
      return {
        current : this.props.current,
        done    : false
      };
    },
    componentWillReceiveProps:function (props){
      // console.log(props.current, props.count);
      this.setState({
        current : props.current,
        done    : props.current == this.props.count
      });
    },
    render:function () {
      var width = (this.state.current / this.props.count * 100) + "%";
      var cl = "b-progress" +
        (this.state.done ? " done" : "");
      return (
        <div className={cl}>
          <div className="b-progress__row">
            <div className="b-progress__row__bar">
              <div className="b-progress__bar">
                <div className="b-progress__bar__load"
                     style={{width:width}}></div>
              </div>
            </div>
            <div className="b-progress__row__count">
              <div className="b-progress__count">{this.state.current} из {this.props.count}</div>
            </div>
          </div>
        </div>
      );
    }
  });

}());
