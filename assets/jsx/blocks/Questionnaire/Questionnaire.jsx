var Questionnaire = (function () {
  'use strict';

  function getAnswerHeight() {
    return (window.innerWidth > 500 ? 136 : 272);
  }

  return React.createClass({
    getInitialState:function () {
      //console.log('init Questionnaire');
      this._current = 0;
      return {
        current       : 0,
        progress      : 0,
        showAnswers   : false,
        top_style     : {},
        bottom_style  : {},
        answer_style  : {
          height : 0
        },
        hide          : false,
        disable       : false,
        answer_hidden : false
      };
    },

    componentDidMount : function () {
      this.__resizeBottom();
      window.addEventListener('resize', this.__resize);
      if (!Controller.animation) {
        Controller.needAnswer();
        this.displayQuestion();
      }
    },
    displayQuestion:function (){
      this.refs.question_view.display();
      var st = this.state;
      st.progress = this._current + 1;
      this.setState(st);
      this.initBottom(true).initAnswers();
    },

    initBottom:function (answers){
      // console.log('Questionnaire initBottom');
      if (answers)
        setTimeout(this.showAnswers, 300);
      return this;
    },

    initAnswers:function (){
      var st = this.state;
      st.answer_hidden = false;
      st.answer_style = {
        height : getAnswerHeight()
      };
      this.setState(st);
      return this;
    },

    __resize:function (){
      this.__resizeAnswer().__resizeBottom();
    },

    __resizeAnswer:function (){
      var st = this.state;
      st.answer_style = {
        height : getAnswerHeight()
      };
      this.setState(st);
      return this;
    },

    __resizeBottom:function (need){
      // console.log('Questionnaire __resizeBottom');
      // console.log(this.refs.container.offsetHeight);
      var st = this.state;

      var winH = window.innerHeight
        , headH = 120
        , topH = this.refs.top.offsetHeight
        , bottomH = this.refs.container.offsetHeight + 60;

      if (winH > headH + topH + bottomH) {
        st.bottom_style = {height : winH - topH - headH + 20};
      } else {
        st.bottom_style = {height : 'auto'};
      }
      this.setState(st);
    },

    showAnswers:function (){
      // console.log('Questionnaire showAnswers');
      var st = this.state;
      st.showAnswers = true;
      this.setState(st);
      this.__resizeBottom();
    },

    onAnswer:function (answer){
      // console.log('Questionnaire onAnswer');
      Controller.answer(answer);
      setTimeout(this.showVideoOnCurrent, 1000);
      SmoothScroll.anim('questionnaire_top');
    },

    showVideoOnCurrent:function (){
      // console.log('Questionnaire showVideoOnCurrent');
      this.refs.question_view.showVideo();
      this.__resizeBottom();
    },

    next:function (){
      // console.log('Questionnaire next');
      if (this.isFinish())
        return this.finish();
      this.refs.question_view.next();
      this._current = this.state.current + 1;
      var st = this.state;
      st.current = this._current;
      st.progress = this._current + 1;
      this.setState(st);
      if (!Controller.animation) {
        Controller.needAnswer();
        this.displayQuestion();
      }
    },

    __resetTop:function (){
    },
    __resizeTop:function (){
    },

    isFinish:function (){
      // console.log('Questionnaire isFinish');
      return this.state.progress == this.props.questions.length;
    },

    finish:function (){
      // console.log('Questionnaire finish');
      this.props.controller.finish();
      this.hide();
    },

    hide:function (){
      var st = this.state;
      st.hide = true;
      this.setState(st);

      setTimeout(this.disable, 500);

    },

    disable:function (){
      var st = this.state;
      st.disable = true;
      this.setState(st);

    },

    render:function () {
      var cur = this.state.current, self = this;

      var answers = (this.state.showAnswers) ?
        (this.props.questions[this.state.current].answers.map(function (answer, index) {
          return (
            <div key={'answer_'+cur+"_"+index} className="b-questionnaire__bottom__answers__row__col">
              <AnswerBtn text={answer.text} right={answer.right} onSelect={self.onAnswer}/>
            </div>
          );
        })) : [];

      var answer_class = 'b-questionnaire__bottom__answers' +
        (this.state.answer_hidden ? " hide" : "");

      var cl = 'b-questionnaire' +
        (this.state.hide ? ' hide' : '') +
        (this.state.disable ? ' disable' : '');
      return (
        <div className={cl}>
          <div ref="top" className="b-questionnaire__top" style={this.state.top_style}>
            <div id="questionnaire_top" className="b-questionnaire__top__question">
              <QuestionsView ref="question_view"
                             controller={this.props.controller}
                             questions={this.props.questions}/>
            </div>
          </div>
          <div ref="bottom" className="b-questionnaire__bottom" style={this.state.bottom_style}>
            <div ref="container" className="b-questionnaire__bottom__container">
              <div className={answer_class} style={this.state.answer_style}>
                <div className="b-questionnaire__bottom__answers__row">
                  {answers}
                </div>
              </div>
              <div className="b-questionnaire__bottom__container__footer">
                <div className="b-questionnaire__bottom__progress">
                  <Progress current={this.state.progress} count="5"/>
                </div>
                <div className="b-questionnaire__bottom__rules">
                  <a href="#" onClick={this.props.controller.showRules} className="b-footer__rules__link"><span
                    className="b-footer__rules__link__inner">Правила конкурса</span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }

  });

}());