var Logo = (function () {
  'use strict';

  return React.createClass({
    render:function () {
      return (
        <div onClick={this.props.controller.start} className="b-logo">
          <div className="b-logo__back">
            <div className="b-logo__back__before">
              <Shape top="5px" left="5px" color="orange"/>
            </div>
            <img src={config.path.img+'main/ball-back.png'} alt="" className="b-logo__back__img"/>
            <div className="b-logo__back__after">
            </div>
          </div>
          <div className="b-logo__front">
            <div className="b-logo__front__before">
              <Shape top="-5px" left="105px" size="1.4" color="red"/>
            </div>
            <img src={config.path.img+'main/ball.png'} alt="" className="b-logo__back__img"/>
            <div className="b-logo__front__after">
              <Shape top="110px" left="15px" size=".9" color="purple"/>
              <Shape top="110px" left="90px" size="1.2" color="green"/>
            </div>
          </div>
        </div>
      );
    }
  });

}());