var Header = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      //console.log('init Header');
      return {
        start   : false,
        hide    : false,
        disable : false,
        display : true
      };
    },
    tick:function (){
      // console.log(arguments);
    },
    start:function (){
      var st = this.state;
      st.start = true;
      this.setState(st);
    },
    hide:function (){
      var st = this.state;
      st.hide = true;
      this.setState(st);
    },
    disable:function (){
      var st = this.state;
      st.disable = true;
      this.setState(st);
      setTimeout(this.displayFalse, 500);
    },
    displayFalse:function (){
      var st = this.state;
      st.display = false;
      this.setState(st);
    },
    render:function () {
      var prize = (this.state.hide && this.state.display) ? <Prize euro="true"/> : '';
      var cl = 'b-header'
        + (this.state.hide ? " hide" : '')
        + (this.state.disable ? " disable" : '');
      var st = {
        // display : (this.state.display ? "block" : "none")
      };
      return (
        <div className={cl} style={st}>
          <div className="b-header__logos">
            <div className="b-header__logos__logo">
              <a href="https://onetwotrip.com" target="__blank" className="b-header__logos__logo__ott">
                <img src={config.path.img+"logo-ott.png"} className="b-header__logos__logo__ott__img"/>
              </a>
              <div className="b-header__logos__logo__uefa">
                <img src={config.path.img+"logo-champ.png"}
                     className="b-header__logos__logo__uefa__img"/></div>
            </div>
            <div className="b-header__logos__prize">
              {prize}
            </div>
          </div>
          <div className="b-header__title">
            <h1 className="b-header__title__heading">Чемпионат Европы по футболу — отличный повод отправиться в
              путешествие!</h1>
          </div>
        </div>
      )
    }
  });
}());
