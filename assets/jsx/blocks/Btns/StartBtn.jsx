var StartBtn = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      //console.log('init StartBtn');
      return {
        disabled : true
      };
    },

    componentDidMount:function () {
      Controller.loadQuestions(this.enable);
    },

    enable:function () {
      this.setState({
        disabled : false
      });
    },

    click:function () {
      this.props.controller.start();
    },

    render:function () {
      return (
        <button id="main-start" className="b-start-btn" disabled={this.state.disabled} onClick={this.click}><span
          className="b-start-btn__inner">Вперед за призами</span></button>
      );
    }
  });
}());
