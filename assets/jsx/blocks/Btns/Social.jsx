var Social = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      //console.log('init Social');
      return {};
    },
    click:function (){
      if (this.props.result) {
        if (this.props.controller) {
          this.props.controller.shareResult(this.props.type);
        } else {
          Controller.shareResult(this.props.type);
        }
      } else
        Controller.share(this.props.type);
    },
    render : function () {
      var cl = 'b-social-icon b-social-icon--' + this.props.type;
      return (
        <div className="b-social-btn" onClick={this.click}>
          <div className={cl}></div>
        </div>
      )
    }
  });
}());
