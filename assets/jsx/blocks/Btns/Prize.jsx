var Prize = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      //console.log('init Prize');
      return {};
    },
    componentDidMount:function () {
    },
    componentWillUnmount:function (){
    },
    render:function () {
      return (
        <div className={"b-prize "+(this.props.euro?'euro':"")}>
          <div className="b-prize__btn">
            <span className="b-prize__btn__box"/>
            <span className="b-prize__btn__inner">Бесплатные путешествия знатокам  футбола</span>
            <div className="b-prize__btn__magic">
              <div className="b-prize__btn__magic__left"></div>
              <div className="b-prize__btn__magic__right"></div>
            </div>
          </div>
        </div>
      );
    }
  });

}());
