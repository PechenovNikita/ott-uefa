var Socials = (function () {
  'use strict';

  return React.createClass({
    render : function () {
      return (
        <div className={"b-socials "+(this.props.blue ? "blue" : "")}>
          <div className="b-socials__row">
            <div className="b-socials__row__col">
              <Social result={this.props.result} type="fb" controller={this.props.controller}/>
            </div>
            <div className="b-socials__row__col">
              <Social result={this.props.result} type="vk" controller={this.props.controller}/>
            </div>
            <div className="b-socials__row__col">
              <Social result={this.props.result} type="tw" controller={this.props.controller}/>
            </div>
            <div className="b-socials__row__col">
              <Social result={this.props.result} type="ok" controller={this.props.controller}/>
            </div>
          </div>
          <div className="b-socials__magic2"></div>
        </div>
      )
    }
  });

}());
