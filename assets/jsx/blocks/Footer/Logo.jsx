var FooterLogo = (function () {
  'use strict';

  return React.createClass({
    render:function () {
      return (
        <div className="b-footer-logo">
          <div className="b-footer-logo__prize">

            <div className="b-footer-prize">
              <div className="b-footer-prize__logo">
                <img src={config.path.img+"prize/box.png"} alt="" className="b-footer-prize__logo__img"/>
              </div>
              <div className="b-footer-prize__text">
                <span className="b-footer-prize__text__inner">сертификат на 50 000 рублей на путешествие.</span>
              </div>
              <div className="b-footer-prize__shapes">
                <SimpleShape color="green" size=".8" top="-50px" left="-10px"/>
                <SimpleShape color="orange" size=".6" top="-40px" left="-30px"/>
                <SimpleShape color="blue" size=".75" top="-20px" left="-20px"/>
                <SimpleShape color="red" size=".9" top="-5px" left="10px"/>
                <SimpleShape color="purple" size=".6" top="-25px" left="40px"/>
              </div>
            </div>
            <div className="b-footer-logo__prize__text">
              <div className="b-footer-logo__prize__text__inner">Главный приз</div>
            </div>
          </div>
          <div className="b-footer-logo__ball">
            <Logo controller={this.props.controller}/>
          </div>
          <div className="b-footer-logo__champ">

            <div className="b-footer-champ">
              <div className="b-footer-champ__logo">
                <img src={config.path.img+"main/champ-logo.png"} alt="" className="b-footer-champ__logo__img logo"/>
                <img src={config.path.img+"main/champ-text.png"} alt="" className="b-footer-champ__logo__img text"/>
              </div>
              <div className="b-footer-champ__shapes">
                <SimpleShape color="purple" size=".8" top="-50px" left="-20px"/>
                <SimpleShape color="blue" size=".75" top="-20px" left="-30px"/>
                <SimpleShape color="green" size=".9" top="-5px" left="0px"/>
                <SimpleShape color="red" size=".6" top="-25px" left="30px"/>
              </div>
            </div>
            <div className="b-footer-logo__champ__text">
              <div className="b-footer-logo__champ__text__inner">Партнер спецпроекта</div>
            </div>

          </div>
        </div>
      );
    }
  });
}());
