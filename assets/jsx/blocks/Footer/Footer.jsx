var Footer = (function () {
  'use strict';

  return React.createClass({
    getInitialState:function () {
      //console.log('init Footer');
      return {
        hide : false,
        logo : false
      };
    },
    hide:function (){
      var st = this.state;
      st.hide = true;
      this.setState(st);
    },
    initLogo:function (){
      var st = this.state;
      st.logo = true;
      this.setState(st);
    },
    render : function () {
      var cl = 'b-footer'
        + (this.state.hide ? ' hide' : '');
      var logo = (this.state.logo ? <FooterLogo controller={this.props.controller}/> : '');
      return (
        <div className={cl}>
          <div className="b-footer__logo">
            {logo}
          </div>
          <div className="b-footer__content">
            <div className="b-footer__content__title">
              <div className="b-footer__content__title__inner">Проверьте свои силы в нашем тесте на знание футбольного
                юмора и собирайте вещи.
              </div>
            </div>
            <div className="b-footer__content__text"><span className="b-footer__content__text__inner">С главным призом – хоть в Париж!</span>
            </div>
            <div className="b-footer__content__btns">
              <StartBtn controller={this.props.controller}/>
            </div>
          </div>
          <div className="b-footer__bottom">
            <div className="b-footer__bottom__copyright">
              &#169; OneTwoTrip
            </div>
            <div className="b-footer__bottom__social">
              <Socials/>
            </div>
            <div className="b-footer__bottom__rules">
              <div className="b-footer__rules"><a href="#" onClick={this.props.controller.showRules}
                                                  className="b-footer__rules__link"><span
                className="b-footer__rules__link__inner">Правила конкурса</span></a></div>
            </div>
          </div>
        </div>
      )
    }
  });

}());
