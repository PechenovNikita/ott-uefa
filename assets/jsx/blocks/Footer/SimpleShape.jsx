var SimpleShape = (function () {
  'use strict';

  var colors = ['red', 'green', 'purple', 'orange', 'blue'];

  return React.createClass({
    getInitialState:function () {
      return {
        color : (this.props.color ? this.props.color : colors[Math.floor(Math.random() * colors.length)])
      };
    },
    render:function () {
      var style = {
        top  : this.props.top,
        left : this.props.left
      };
      if (this.props.size) {
        style.width = 34 / 2 * this.props.size;
        style.height = 40 / 2 * this.props.size;
      }
      return (
        <div style={style} className={"b-simple-shape " + this.state.color}></div>
      );
    }
  });

}());
