var Ajax = function () {

  "use strict";

  function makeUrlString(url, data) {
    var started = false;
    if (url.indexOf('?') > 0)
      started = true;
    for (var name in data) {
      if (data.hasOwnProperty(name)) {
        // formData.append("name", data[name]);
        if (!started)
          url += "?" + encodeURIComponent(name) + "=" + encodeURIComponent(data[name]);
        else
          url += "&" + encodeURIComponent(name) + "=" + encodeURIComponent(data[name]);
      }
    }
    return url;
  }

  function loadXMLDoc(url, data, callback, type) {
    type = (typeof type == "undefined") ? 'GET' : type;
    var xmlhttp;

    if (window.XMLHttpRequest) {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else {
      // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == XMLHttpRequest.DONE) {
        if (xmlhttp.status == 200) {
          callback(xmlhttp.responseText);
        }
        else if (xmlhttp.status == 400) {
          // console.log('There was an error 400');
        }
        else {
          // console.log('something else other than 200 was returned');
        }
      }
    };

    // xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    var d = ((data) ? "" : "");

    // var formData = new FormData();

    for (var name in data) {
      if (data.hasOwnProperty(name)) {
        // formData.append("name", data[name]);
        if (d == "")
          d += encodeURIComponent(name) + "=" + encodeURIComponent(data[name]);
        else
          d += "&" + encodeURIComponent(name) + "=" + encodeURIComponent(data[name]);
      }
    }

    xmlhttp.open(type, url, true);
    xmlhttp.send(d);
  }

  return {
    getJson:function(url, callback){
      loadXMLDoc(url, null, function (response) {
        callback(JSON.parse(response));
      });
    },
    post:function(url, data, callback){
      loadXMLDoc(url, data, function (response) {
        callback(JSON.parse(response));
      }, 'POST');
    },
    jsonP:function(url, data, callback){
      var name = 'jsonP_' + Date.now();
      window[name] = function (response) {
        callback(response);
      };
      var script = document.createElement('script');
      url = makeUrlString(url, data);
      url = makeUrlString(url, {callback : name});
      script.src = url;

      // console.log(url);

      document.getElementsByTagName('head')[0].appendChild(script);
    }
  };
}();
