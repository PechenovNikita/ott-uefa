var SmoothScroll = (function () {
  /**
   by Nemes Ioan Sorin - not an jQuery big fan
   therefore this script is for those who love the old clean coding style
   @id = the id of the element who need to bring  into view

   Note : this demo scrolls about 12.700 pixels from Link1 to Link3
   */
  "use strict";

  var tm;

  function scrollToTop(scrollDuration, to) {
    const scrollHeight = window.scrollY,
      scrollStep = Math.PI / ( scrollDuration / 15 ),
      cosParameter = scrollHeight / 2;
    var scrollCount = 0,
      scrollMargin;
    requestAnimationFrame(step);
    function step() {
      setTimeout(function () {
        if (window.scrollY != 0) {
          requestAnimationFrame(step);
          scrollCount = scrollCount + 1;
          scrollMargin = cosParameter - cosParameter * Math.cos(scrollCount * scrollStep);
          window.scrollTo(0, ( scrollHeight - scrollMargin ));
        }
      }, 15);
    }
  }

  return {
    anim : function (id) // the main func
    {
      scrollToTop(500, 120);
    }
  };

}());
