var config = (function () {
  "use strict";
  return {
    confirm     : 'https://www.onetwotrip.com/_api/visitormanager/registerPromo/',
    valideEmail : 'https://www.onetwotrip.com/_api/emailvalidator/validate/',
    url         : 'http://www.onetwotrip.com/promo/euro-2016/',
    redirectURL : 'http://www.onetwotrip.com/promo/euro-2016/confirm.html',
    source      : 'promo_eurofootball2016',
    path        : {
      img : '/promo/euro-2016/assets/img/',
      data: '/promo/euro-2016/assets/questions/data.json'
    }
  };
}());