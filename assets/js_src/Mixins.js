var mixins = (function () {
  "use strict";

  var pfx = ["webkit", "moz", "MS", "o", ""];

  function AddPrefixEvent(element, type, callback) {
    for (var p = 0; p < pfx.length; p++) {
      if (!pfx[p]) type = type.toLowerCase();
      element.addEventListener(pfx[p] + type, callback, false);
    }
  }

  function RemovePrefixEvent(element, type, callback) {
    for (var p = 0; p < pfx.length; p++) {
      if (!pfx[p]) type = type.toLowerCase();
      element.removeEventListener(pfx[p] + type, callback, false);
    }
  }

  return {
    addAniEnd:function(element, callback){
      AddPrefixEvent(element, 'AnimationEnd', callback);
    },
    removeAniEnd:function(element, callback){
      RemovePrefixEvent(element, 'AnimationEnd', callback);
    }
  };
}());
