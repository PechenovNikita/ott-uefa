var ImageLoader = (function () {

  "use strict";
  /**
   *
   * @param sources
   * @constructor
   */
  function LoaderImages(sources) {
    this._sources = sources;
    this._count = sources.length;
    this._onLoad = undefined;

    this.__load();
  }

  LoaderImages.prototype = {
    done:function(callback){
      if (this._count == 0)
        callback();
      else
        this._onLoad = callback;
      return this;
    },
    __whenImageLoad:function(){
      this._count--;
      if (this._onLoad && this._count == 0)
        this._onLoad();
      else
        this.__loadOne(this._sources.shift());
    },
    __loadOne:function(src){
      if (typeof src === 'undefined')
        return;
      var tempImg = new Image();
      tempImg.onload = this.__whenImageLoad.bind(this);
      tempImg.src = config.path.img + src;
    },
    __load:function(){
      var i = 0;
      while (i < 4 && this._sources.length > 0) {
        this.__loadOne(this._sources.shift());
        i++;
      }
      return this;
    }
  };

  return function (srcs) {
    return new LoaderImages(srcs);
  };
}());
