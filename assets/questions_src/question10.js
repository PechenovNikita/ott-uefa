var config = require('./../../gulp/_config');

module.exports = {
  question : "Как отреагировал Георгий Черданцев на гол, забитый Россией голландцам в ¼ чемпионата Европы 2008 года?",
  answers  : [
    {
      text  : "«Я щас накажу вообще всех!»",
      right : false
    },
    {
      text  : "«Я щас прикончу вообще всех!»",
      right : false
    },
    {
      text  : "«Я щас закончу вообще всё!»",
      right : true
    },
    {
      text  : "«Я щас домой пойду уже вообще!»",
      right : false
    }
  ],
  photo    : config.path.img + 'questions/question-10.jpg',
  videoPoster : config.path.video + 'Question_10.jpg',
  video    : [{
    src  : config.path.video + 'Question_10.mp4',
    type : 'video/mp4'
  }, {
    src  : config.path.video + 'Question_10.webm',
    type : 'video/webm'
  }, {
    src  : config.path.video + 'Question_10.ogv',
    type : 'video/ogv'
  }]
};
