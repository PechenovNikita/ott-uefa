var config = require('./../../gulp/_config');

module.exports = {
  question : "О чём, по мнению комментатора Георгия Черданцева, думал Денис Колодин, когда бил по воротам во время матча?",
  answers  : [
    {
      text  : "«Давай, до свидания!»",
      right : false
    },
    {
      text  : "«Приятного аппетита!»",
      right : false
    },
    {
      text  : "«Добрый вечер!»",
      right : true
    },
    {
      text  : "«Рад встрече!»",
      right : false
    }
  ],
  photo    : config.path.img + 'questions/question-8.jpg',
  videoPoster : config.path.video + 'Question_8.jpg',
  video    : [{
    src  : config.path.video + 'Question_8.mp4',
    type : 'video/mp4'
  }, {
    src  : config.path.video + 'Question_8.webm',
    type : 'video/webm'
  }, {
    src  : config.path.video + 'Question_8.ogv',
    type : 'video/ogv'
  }]
};
