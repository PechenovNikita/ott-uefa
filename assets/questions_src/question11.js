var config = require('./../../gulp/_config');

module.exports = {
  question : "Однажды, по мнению Владимира Стогниенко, Дирк Кюйт, «стоя на коленях, обрабатывал мяч…»",
  answers  : [
    {
      text  : "со знанием дела",
      right : false
    },
    {
      text  : "верой и правдой",
      right : false
    },
    {
      text  : "резинкой от трусов",
      right : true
    },
    {
      text  : "чем мог",
      right : false
    }
  ],
  photo    : config.path.img + 'questions/question-11.jpg',
  videoPoster : config.path.video + 'Question_11.jpg',
  video    : [{
    src  : config.path.video + 'Question_11.mp4',
    type : 'video/mp4'
  }, {
    src  : config.path.video + 'Question_11.webm',
    type : 'video/webm'
  }, {
    src  : config.path.video + 'Question_11.ogv',
    type : 'video/ogv'
  }]
};
