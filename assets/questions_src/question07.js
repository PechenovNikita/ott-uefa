var config = require('./../../gulp/_config');

module.exports = {
  question    : "Как однажды охарактеризовал Роман Нагучев передачу пяткой Ван дер Варта?",
  answers     : [
    {
      text  : "«Шиии... Шикарно!»",
      right : false
    },
    {
      text  : "«Жеее... Железно!»",
      right : false
    },
    {
      text  : "«Блеее… Блестяще!»",
      right : true
    },
    {
      text  : "«Оху… Ох, уж этот Ван дер Варт!»",
      right : false
    }
  ],
  photo       : config.path.img + 'questions/question-7.jpg',
  videoPoster : config.path.video + 'Question_7.jpg',
  video       : [{
    src  : config.path.video + 'Question_7.mp4',
    type : 'video/mp4'
  }, {
    src  : config.path.video + 'Question_7.webm',
    type : 'video/webm'
  }, {
    src  : config.path.video + 'Question_7.ogv',
    type : 'video/ogv'
  }]
};
