var config = require('./../../gulp/_config');

module.exports = {
  question : "Как Георгий Черданцев прокомментировал действия Жан Луиджи Буффона, отразившего два сложных удара подряд?",
  answers  : [
    {
      text  : "«Друзья, мы с вами смотрим футболище!»",
      right : false
    },
    {
      text  : "«Луиджище! Ужаснейший Луиджище!»",
      right : false
    },
    {
      text  : "«Буффонище! Чудовищный Буффонище!»",
      right : true
    },
    {
      text  : "«Чудовище в чёрной футболке!»",
      right : false
    }
  ],
  photo    : config.path.img + 'questions/question-9.jpg',
  videoPoster : config.path.video + 'Question_9.jpg',
  video    : [{
    src  : config.path.video + 'Question_9.mp4',
    type : 'video/mp4'
  }, {
    src  : config.path.video + 'Question_9.webm',
    type : 'video/webm'
  }, {
    src  : config.path.video + 'Question_9.ogv',
    type : 'video/ogv'
  }]
};
