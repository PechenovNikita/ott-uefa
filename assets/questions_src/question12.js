var config = require('./../../gulp/_config');

module.exports = {
  question : "Какой суперспособностью, по мнению Владимира Стогниенко, обладают итальянские игроки?",
  answers  : [
    {
      text  : "обрушиваются всей командой на арбитра и уговаривают не давать жёлтую карточку",
      right : false
    },
    {
      text  : "бегают в полтора раза быстрее обычных людей",
      right : false
    },
    {
      text  : "каждый итальянец в команде может добавить 20 секунд дополнительного времени",
      right : false
    },
    {
      text  : "после контакта с ногой соперника могут пробежать еще четыре шага",
      right : true
    }
  ],
  photo    : config.path.img + 'questions/question-12.jpg',
  videoPoster : config.path.video + 'Question_12.jpg',
  video    : [{
    src  : config.path.video + 'Question_12.mp4',
    type : 'video/mp4'
  }, {
    src  : config.path.video + 'Question_12.webm',
    type : 'video/webm'
  }, {
    src  : config.path.video + 'Question_12.ogv',
    type : 'video/ogv'
  }]
};
