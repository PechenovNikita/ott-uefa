var config = require('./../../gulp/_config');

//https://www.facebook.com/sharer/sharer.php?u=https://meduza.io/embed/house-of-football-game/result%3FimageKey%3D1-3%26version%3Dv3

module.exports = {
  question    : "По мнению Василия Уткина, имя футболиста Ибая Гомеса звучит как ...",
  answers     : [
    {
      text  : "возглас",
      right : false
    },
    {
      text  : "приговор",
      right : false
    },
    {
      text  : "призыв",
      right : true
    },
    {
      text  : "диагноз",
      right : false
    }
  ],
  photo       : config.path.img + 'questions/question-13.jpg',
  videoPoster : config.path.video + 'Question_13.jpg',
  video       : [{
    src  : config.path.video + 'Question_13.mp4',
    type : 'video/mp4'
  }, {
    src  : config.path.video + 'Question_13.webm',
    type : 'video/webm'
  }, {
    src  : config.path.video + 'Question_13.ogv',
    type : 'video/ogv'
  }]
};
