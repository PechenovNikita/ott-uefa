var gulp = require('gulp')
  , watch = require('gulp-watch');

var jeditor = require("gulp-json-editor");
var _config = require('./_config');

gulp.task('question-generate', function () {
  gulp.src('./assets/questions_src/data.json')
    .pipe(jeditor(require('./../assets/questions_src/qlist')))
    .pipe(gulp.dest('./assets/questions'));
});

gulp.task('question-watch', function () {
  watch('./assets/questions/**/*.js', function () {
    gulp.src('./assets/questions_src/data.json')
      .pipe(jeditor(require('./../assets/questions_src/qlist')))
      .pipe(gulp.dest('./assets/questions'));
  });
});

module.exports = {
  tasks : ['question-generate'],
  watch : ['question-watch']
};