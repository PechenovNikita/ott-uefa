var gulp = require('gulp')
  , less = require('gulp-less')
  , watch = require('gulp-watch')
  , livereload = require('gulp-livereload');

var sourcemaps = require('gulp-sourcemaps')
  , lessData = require("gulp-less-data");

var _config = require('./_config');

var less_config = {
  pathImg   : '"' + require('./_config').path.img + '"',
  pathFonts : '"' + require('./_config').path.fonts + '"'
};

gulp.task('less-generate', function () {
  gulp.src('./assets/less/assets/reset.less')
    .pipe(less())
    .pipe(gulp.dest('./assets/css'));
  
  return gulp.src('./assets/less/index.less')
    .pipe(sourcemaps.init({debug : true}))
    .pipe(lessData(less_config))
    .pipe(less())
    .pipe(gulp.dest('./assets/css'))
    .pipe(livereload());
});

gulp.task('less-watch', function () {
  watch('./assets/less/**/*.less', function () {
    gulp.src('./assets/less/index.less')
      .pipe(sourcemaps.init({debug : true}))
      .pipe(lessData(less_config))
      .pipe(less())
      .pipe(gulp.dest('./assets/css'))
      .pipe(livereload());
  });
});

module.exports = {
  tasks : ['less-generate'],
  watch : ['less-watch']
};