var gulp = require('gulp')
  , babel = require('gulp-babel')
  , rename = require('gulp-rename')
  , uglify = require('gulp-uglify')
  , sourcemaps = require('gulp-sourcemaps')
  , concat = require('gulp-concat');

var mustache = require("gulp-mustache");

var _config = require('./_config');

gulp.task('js-config-generate', function () {
  return gulp.src("./assets/templates/config.mustache")
    .pipe(mustache({
      confirm     : _config.confirm,
      valideEmail : _config.valideEmail,
      url         : _config.url,
      redirectURL : _config.redirectURL,
      source      : _config.source,
      pathImg     : _config.path.img,
      pathData    : _config.path.data
    }))
    .pipe(rename('config.js'))
    .pipe(gulp.dest("./assets/js_src"));
});

gulp.task('js-concat', ['js-config-generate'], function () {
  return gulp.src(['./assets/js_src/config.js', './assets/js_src/Mixins.js', './assets/js_src/Scroll.js', './assets/js_src/Ajax.js', './assets/js_src/Share.js', './assets/js_src/ImageLoader.js'

      , './assets/jsx/blocks/**/*.jsx'
      , './assets/jsx/View.jsx'

      , './assets/js_src/Controller.js'
      , './assets/js_src/App.js'])

    .pipe(sourcemaps.init())
    .pipe(babel({
      presets : ['es2015', 'react']
    }))
    .pipe(concat('uefa.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./assets/js'));
});

module.exports = {
  tasks : ['js-concat']
};