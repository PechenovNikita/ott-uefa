var path = '/promo/euro-2016/';
// var path = '/ott/football/';

var currentDomain = 'http://www.onetwotrip.com';
// var currentDomain = 'http://dev.zachot.com';

module.exports = {
  confirm     : 'https://www.onetwotrip.com/_api/visitormanager/registerPromo/',
  valideEmail : 'https://www.onetwotrip.com/_api/emailvalidator/validate/',
  url         : currentDomain + path,
  redirectURL : currentDomain + path + 'confirm.html',
  source      : 'promo_eurofootball2016',
  path        : {
    src   : path,
    js    : path + 'assets/js/',
    css   : path + 'assets/css/',
    fonts : path + 'assets/fonts/',
    img   : path + 'assets/img/',
    video : path + 'assets/video/',
    data  : path + 'assets/questions/data.json'
  }
};