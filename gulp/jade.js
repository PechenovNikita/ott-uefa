var gulp = require('gulp')
  , jade = require('gulp-jade')
  , watch = require('gulp-watch')
  , livereload = require('gulp-livereload');

var _config = require('./_config');

var jade_config = {
  pretty : true,
  locals : {
    config : _config
  }
};

gulp.task('jade-result-generate', function () {
  return gulp.src('./assets/jade/results/*.jade')
    .pipe(jade(jade_config))
    .pipe(gulp.dest('./results'))
    .pipe(livereload());
});

gulp.task('jade-generate', function () {
  return gulp.src(['./assets/jade/index.jade', './assets/jade/confirm.jade'])
    .pipe(jade(jade_config))
    .pipe(gulp.dest('./'))
    .pipe(livereload());
});

gulp.task('jade-watch', function () {
  watch(['./assets/jade/results/*.jade', './assets/jade/og.jade', './assets/jade/layout-result.jade'], function () {
    gulp.src('./assets/jade/results/*.jade')
      .pipe(jade(jade_config))
      .pipe(gulp.dest('./results'))
      .pipe(livereload());
  });
  watch(['./assets/jade/**/*.jade', '!./assets/jade/layout-result.jade', '!./assets/jade/results/*.jade'], function () {
    gulp.src(['./assets/jade/index.jade', './assets/jade/test.jade', './assets/jade/confirm.jade'])
      .pipe(jade(jade_config))
      .pipe(gulp.dest('./'))
      .pipe(livereload());
  });
});

module.exports = {
  tasks : ['jade-generate', 'jade-result-generate'],
  watch : ['jade-watch']
};