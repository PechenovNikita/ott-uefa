var gulp = require('gulp')
  , jade = require('gulp-jade')
  , livereload = require('gulp-livereload');

var runSequence = require('run-sequence');

gulp.task('jade', require('./gulp/jade').tasks);

gulp.task('less', require('./gulp/less').tasks);

gulp.task('js', require('./gulp/js').tasks);

gulp.task('json', require('./gulp/question').tasks);

gulp.task('_build', function () {
  return runSequence(['js', 'json', 'jade', 'less']);
});

gulp.task('_watch', function () {
  livereload.listen();

  gulp.start(require('./src/gulp/question').watch);
  gulp.start(require('./src/gulp/jade').watch);
  gulp.start(require('./src/gulp/less').watch);

});

gulp.task('default', ['_build'], function () {
  gulp.start('_watch');
});